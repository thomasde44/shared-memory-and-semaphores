
#include "shared_mem.h"
#include <ctype.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <vector>



int main () {
    char name[] = "shm1";
    shmbuf * shm_a = call_shared_memory(name);
    std::vector<pthread_t> thread_container;


    std::string args[4];
    args[0] = "0";
    args[1] = "0";
    args[2] = "0";
    args[3] = name;

    int tmp = 0;
    std::string tmp_str;
    while (true) {
        char answer_1[23];
        int delay_time = 0;
        std::cout << "Would you like to create a writer thread? ";
        std::cin >> answer_1;
        if (strncmp(answer_1, "y", 23) == 0) {
            std::cout << "What is the delay time for this thread? ";
            std::cin >> delay_time;
            args[2] = std::to_string(delay_time);
            tmp = atoi( args[0].c_str() );
            tmp++;

            tmp_str = std::to_string(tmp);
            args[0] = tmp_str;
            pthread_t tmp2;
            int rc = pthread_create(&tmp2, NULL, post_to_shm, args);
            if (rc != 0) {
                std::cout << "thread dead" << std::endl;
                errExit("pthread_create");
            }
        }
        if (strncmp(answer_1, "n", 23) == 0) {
            std::cout << "Exiting" << std::endl;
            exit(0);
        }
    }

    return 0;
}
