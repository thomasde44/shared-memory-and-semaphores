# Shared memory and semaphores

### fork_processes.c ###
This program spawns new processes with fork(). It creates a text file with the process id of the new process used as the files name. Inside the file it saves a string that was entered by the user. When the user inputs Done the program quits.
