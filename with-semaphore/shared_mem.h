#include <sys/mman.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>


#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \
                        } while (0)

/* Define a structure that will be imposed on the shared
    memory object */

sem_t mutex;

struct shmbuf {

    int thread_id;
    int report_id;
    int time_interval;
};


// returns file desfriptor of shared memory
shmbuf * call_shared_memory(std::string name) {

    int fd = shm_open(name.c_str(), O_CREAT | O_RDWR, 0666);

    if (fd == -1) {
        errExit("shm_open");
    }

    if (ftruncate(fd, sizeof(struct shmbuf)) == -1) {
        errExit("ftruncate");
    }

    struct shmbuf *shmp = (shmbuf*)mmap(NULL, sizeof(*shmp), PROT_READ | PROT_WRITE,
                                          MAP_SHARED, fd, 0);
    
    if (shmp == MAP_FAILED) {
        errExit("mmap");
    }
    

    return shmp;        

}


sem_t * call_semaphore(std::string name) {


    sem_t * sem = sem_open(name.c_str(), O_CREAT | O_RDWR, 0666, 1);

    if (sem == SEM_FAILED) {
        errExit("sem_open");
    }

    return sem;

}


void * post_to_shm(void * ptr) {

    std::string * info = (std::string *) (ptr);

    int report_cnt = 0;

    shmbuf * shmp = call_shared_memory(info[3]);

    int sleep_interval = std::stoi(info[2]);
    int tid = std::stoi(info[0]);
    // std::cout << "thread id: " << shmp->thread_id << std::endl;
    while (1) {
        int value;

        sem_post(&mutex);
        report_cnt += 1;
        shmp->report_id = report_cnt;
        shmp->thread_id = tid;
        shmp->time_interval = sleep_interval;
        std::cout << shmp->report_id << " " << shmp->thread_id << " " << shmp->time_interval << std::endl;
        sleep(sleep_interval);
        sem_wait(&mutex);
    }

}
