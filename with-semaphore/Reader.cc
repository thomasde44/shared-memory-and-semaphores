#include "shared_mem.h"


int main() {

    char name[] = "shm1";

    shmbuf * shm_a = call_shared_memory(name);
    while(1) {
        // call shared memory
        int value;
        sem_post(&mutex);

        std::cout << shm_a->report_id << " " << shm_a->thread_id << " " << shm_a->time_interval << std::endl;
        sem_wait(&mutex);
        
    }

}
