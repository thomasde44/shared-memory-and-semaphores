#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

int main() {

    char user_data[256];
    while(1) {
        printf("Enter a string: ");
        fgets(user_data, 256 , stdin);
        int pid = fork(); 
        if (pid) {
            FILE * fp;
            char a[10];
            sprintf(a, "%d.txt", pid);
            fp = fopen(a, "w+");
            fprintf(fp, "%s\n", user_data);  
            kill(pid, SIGKILL);
        }
        if (strncmp(user_data, "Done\n", 256) == 0) {
            return 0;
        }
        sleep(0.5);
    }

    return 0;
}
